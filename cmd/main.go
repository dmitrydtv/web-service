package main

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

func hello(w http.ResponseWriter, req *http.Request) {

	fmt.Fprintf(w, "hello STAS\n 2+2 = %d\n", 2+2)
}

func headers(w http.ResponseWriter, req *http.Request) {

	for name, headers := range req.Header {
		for _, h := range headers {
			fmt.Fprintf(w, "%v: %v\n", name, h)
		}
	}
}

func adder(w http.ResponseWriter, req *http.Request) {

	a := req.FormValue("a")
	b := req.FormValue("b")
	var c int

	host := req.Host
	//port := req.URL.Port()

	port := strings.ReplaceAll(host, "localhost:", "")

	fmt.Fprintf(w, "Запрос выполнен на порт: %s\n", port)

	aInt, err := strconv.Atoi(a)
	if err != nil {
		fmt.Println(err)
		fmt.Fprintf(w, "%s", "Что-то пошло не так :(\n")
		return
	}

	bInt, err := strconv.Atoi(b)
	if err != nil {
		fmt.Println(err)
		fmt.Fprintf(w, "%s", "Что-то пошло не так :(\n")
		fmt.Fprintf(w, "%s", err)
		return
	}

	c = aInt + bInt

	fmt.Fprintf(w, "%d + %d = %d\n", aInt, bInt, c)

	for name, headers := range req.Header {
		for _, h := range headers {
			fmt.Fprintf(w, "%v: %v\n", name, h)
		}
	}
	fmt.Fprintf(w, "%s\n", req)

	fmt.Fprintf(w, "RequestURI: %s\n", req.RequestURI)
	fmt.Fprintf(w, "Host: %s\n", req.Host)
	fmt.Fprintf(w, "RemoteAddr: %s\n", req.RemoteAddr)

}

func main() {

	http.HandleFunc("/hello", hello)
	http.HandleFunc("/headers", headers)
	http.HandleFunc("/adder", adder)

	go http.ListenAndServe(":5555", nil)
	http.ListenAndServe(":12345", nil)

}
